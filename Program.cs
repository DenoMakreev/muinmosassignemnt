﻿using System;
using System.Diagnostics;
using System.IO;

class Program
{
    static void Main(string[] args)
    {
        string filePath = "supposedLogFile.txt";
        int numberOfLines = 10000;
        
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        using (StreamWriter writer = new StreamWriter(filePath))
        {
            for (int i = 1; i <= numberOfLines; i++)
            {
                writer.WriteLine($"Line {i} from the log file. I guess a log line should be kind of long so I am just making it longer. That should be enough I guess.");
            }
        }

        stopwatch.Stop();
        Console.WriteLine($"It took pretty much {stopwatch.ElapsedMilliseconds} ms. to write {numberOfLines} lines in the file");
        

        // Note: Was not sure in what order you need the lines so there is a different way to add them to the file in the reverse order so you have the latest line on top
        // Might be cheating since I am writing them all at the same time though. You decide!
        string filePathReverse = "supposedLogFileReverse.txt";
        List<string> lines = new List<string>(numberOfLines);

        Stopwatch stopwatchTheSecond = new Stopwatch();
        stopwatchTheSecond.Start();

        for (int i = 1; i <= numberOfLines; i++)
        {
            lines.Add($"Line {i} from the log file. I guess a log line should be kind of long so I am just making it longer. That should be enough I guess.");
        }

        lines.Reverse();

        using (StreamWriter writer = new StreamWriter(filePathReverse))
        {
            foreach (var line in lines)
            {
                writer.WriteLine(line);
            }
        }

        stopwatchTheSecond.Stop();
        Console.WriteLine($"It took pretty much {stopwatchTheSecond.ElapsedMilliseconds} ms. to write {numberOfLines} lines in the file. This time latest on top though");
    }
}